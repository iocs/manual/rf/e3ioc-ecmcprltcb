##############################################################################
## EtherCAT Motion Control Phase Reference Line Temperature Control Box IOC configuration

##############################################################################
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="PRL2")")
require std 3.6.1
require calc

# Set paramaters
epicsEnvSet("P",    "PRL2:")
epicsEnvSet("SLAVE_NUM", 10) # probably change this to be loaded in ecmcPRL
epicsEnvSet("HW_TYPE_IN", "EL3202-0010") # same as above
epicsEnvSet("INP",    "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s$(SLAVE_NUM)-$(HW_TYPE_IN)-AI")
epicsEnvSet("INPA",    "$(INP)1")
epicsEnvSet("INPB",    "$(INP)2")

epicsEnvSet("HW_TYPE_OUT", "EL2502") #
epicsEnvSet("SLAVE_NUM_OUT", "2") #
epicsEnvSet("OUT", "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s$(SLAVE_NUM_OUT)-$(HW_TYPE_OUT)-BO1")

iocshLoad("$(E3_CMD_TOP)/iocsh/PID_test_hw.iocsh", "P=$(P)")

iocInit()

